<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create(){
        return view('partial.postkotak');
      
}
        
    public function store(Request $request){
        $request -> validate([
            'post' => 'required',
            'user_id' => 'required',
        ]);

    DB::table('post')->insert([
        'post' => $request['post'],
        'user_id' => $request['user_id'],

        
    ]);
        return redirect('/partial/postkotak');
    }

    
}
