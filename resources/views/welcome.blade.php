<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Connect Plus</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href=" {{asset('connect/assets/vendors/mdi/css/materialdesignicons.min.css')}} ">
    <link rel="stylesheet" href="{{asset('connect/assets/vendors/flag-icon-css/css/flag-icon.min.css ')}}">
    <link rel="stylesheet" href="{{asset('connect/assets/vendors/css/vendor.bundle.base.css ')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/v4-shims.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('connect/assets/vendors/font-awesome/css/font-awesome.min.css ')}}" />
    <link rel="stylesheet" href="{{asset('connect/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css ')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('connect/assets/css/style.css')}} ">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('connect/assets/images/favicon.png')}}" />
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @include('partial.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
    @include('partial.side')
        <!-- partial -->
       
        <div class="main-panel ">
          <div class="content-wrapper">
            <div class="row" id="proBanner">
              <div class="col-12">
       
                    <div class="row">
                      <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    {{-- heder --}}
                                    @yield('create')
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="card mt-2"> 
                            <div class="card-body" >
                                <div class="row">
                                    <div class="col-sm-12">
                                        {{-- heder --}}
                                        @yield('content')
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          
            
          <!-- partial:partials/_footer.html -->
          <footer class="footer">
            <div class="footer-inner-wraper">
              <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard templates</a> from Bootstrapdash.com</span>
              </div>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('connect/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src=" {{asset('connect/assets/vendors/chart.js/Chart.min.js')}} "></script>
    <script src="{{asset('assets/vendors/jquery-circle-progress/js/circle-progress.min.js ')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('connect/assets/js/off-canvas.js ')}}"></script>
    <script src="{{asset('connect/assets/js/hoverable-collapse.js ')}}"></script>
    <script src="{{asset('connect/assets/js/misc.js ')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{asset('connect/assets/js/dashboard.js ')}}"></script>
    <!-- End custom js for this page -->
  </body>
</html>